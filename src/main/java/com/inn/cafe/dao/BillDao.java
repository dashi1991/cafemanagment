package com.inn.cafe.dao;

import com.inn.cafe.model.Bill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface BillDao extends JpaRepository<Bill,Integer> {

    List<Bill> getAllBills();
    List<Bill> getBillsByUsername(@RequestParam("username") String username);
}
