package com.inn.cafe.service;

import com.inn.cafe.wrapper.UserWrapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

public interface UserService {
    public ResponseEntity<String> signUp(Map<String, String> requestMap);
    public ResponseEntity<String> login(Map<String,String>requestMap);

    public ResponseEntity<List<UserWrapper>> getAllUsers();

    public  ResponseEntity<String> update(Map<String, String> requestMap);

    public ResponseEntity<String> checToken();

    public ResponseEntity<String> changePassword(Map<String, String> requestMap);
}
